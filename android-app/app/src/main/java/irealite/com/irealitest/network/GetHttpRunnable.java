package irealite.com.irealitest.network;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


/**
 * A Runnable that makes a GET request to a given UR an returns the response as a string.
 */
public class GetHttpRunnable implements Runnable {

    private HttpClient httpClient;
    private HttpGet httpGet;
    private String response;
    private boolean done = false;

    public GetHttpRunnable(String url) {
        httpClient = new DefaultHttpClient();
        httpGet = new HttpGet(url);
    }

    @Override
    public void run() {
        HttpResponse httpResponse;
        try {
            httpResponse = httpClient.execute(httpGet);
            InputStream response = httpResponse.getEntity().getContent();
            java.util.Scanner s = new java.util.Scanner(response)
                    .useDelimiter("\\A");
            this.response = s.hasNext() ? s.next() : "";
        } catch (ClientProtocolException e) {
             // Silent catch
        } catch (IOException e) {
            // Silent catch
        }
        this.done = true;
    }

    public boolean isDone() {
        return done;
    }

    public String getResponse() {
        return response;
    }

}