package irealite.com.irealitest.network;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Queries an url, gets the json response and sets into the given textview the obtained result.
 */
public class MessageGetter {

    private final String url;
    private final TextView textViewToUpdate;

    public MessageGetter(String url, TextView textViewToUpdate) {
        this.url = url;
        this.textViewToUpdate = textViewToUpdate;
    }

    public void updateMessage() {
       // Step 1: get the response at the given URL
       GetHttpRunnable getHttpRunnable = new GetHttpRunnable(url);
       Thread th = new Thread(getHttpRunnable);
        th.start();
        // Maybe this should be done asynchronously, in a separated Thread ?

        // Step 2: wait for an answer
        while (!getHttpRunnable.isDone()) {
            // Wait for response
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // Silent catch
            }
        }

        // Step 3: convert response in JSON
        Collection<JSONObject> responseAsJSonObjects = null;
        try {
            responseAsJSonObjects = convertResponseToJSon(getHttpRunnable.getResponse());

            // Step 4: build the string result from JSON
            StringBuilder result = new StringBuilder();
            for (JSONObject jsonObject : responseAsJSonObjects) {
                String message = (String) jsonObject.get("message");
                result.append(message + "\n");
            }

            // Step 5: update the textView
            textViewToUpdate.setText(result.toString());
        } catch (JSONException e) {
           throw new RuntimeException(e);
        }

    }

    private Collection<JSONObject> convertResponseToJSon(String response) throws JSONException {
        Collection<JSONObject> jsonObjects = new LinkedHashSet<JSONObject>();

        // Consider that the response contains a json array
        JSONArray array = new JSONArray(response);
        for (int i = 0; i < array.length(); i++) {
            JSONObject jsonObject = (JSONObject) array.get(i);
            jsonObjects.add(jsonObject);
        }
        return jsonObjects;
    }


}
